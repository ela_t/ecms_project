# README #

Python == 2.7.6

Instalacja projektu lokalnie:

1: Tworzymy katalog o nazwie ECMS

2: Będąc w katalogu ECMS przygotowujemy wirtualne środowisko:

	virtualenv ECMSHV

3:  Przechodzimy do katalogu ECMSHV i aktywujemy środowisko virtualenv poleceniem:

	source bin/activate

4: Pobieramy pliki projektu z repozytorium: 

	git clone git@bitbucket.org:ela_t/ecms_project.git
	
5: Przechodzimy do głównego katalogu projektu i instalujemy w środowisku potrzebne moduły. Korzystamy z pliku requirements.txt pobranego z repozytorium:

	pip install -r requirements.txt
	
Uruchamianie projektu:

1: Uruchamiamy serwer:

	python manage.py runserver
